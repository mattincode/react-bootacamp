import React from "react";
import PropTypes from 'prop-types';

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {news: ""}
    this.OnRefreshClicked = this.OnRefreshClicked.bind(this);
  }

  OnRefreshClicked() {
    fetch("https://rss.aftonbladet.se/rss2/small/pages/sections/nyheter/")
      .then(response => response.text())
      .then(str => new window.DOMParser().parseFromString(str, "text/xml")) 
      .then(data => {        
        //console.log(data);
        let newsList = [];
        let items = data.querySelectorAll("item");
        items.forEach(item => {
          let title = item.querySelector("title").childNodes[0].data;
          newsList.push(title);
        });
        this.setState({news: newsList.join(", ")});
        // TODO: Parse items with title, description, link, image and display in a sub-component
        //... then add like-button... and save the liked state... hint use the guid as key
      });    
  }

  // <img src="https://imgs.aftonbladet-cdn.se/v2/images/108a24a1-75e3-4641-b778-aa1e41fc6e14?fit=crop&h=356&q=50&w=830&s=89637b647fcb5b040a6b1ea95c224e89e9ba55e4" alt=""/>
  render() {
    return (
      <div>
        <div>Hello from Card! {this.props.name}</div>
        <button type="button" onClick={this.OnRefreshClicked}>Refresh</button>
        <div>{this.state.news}</div>        
      </div>
    );
  }
}

Card.defaultProps = {
  name: "test"
};

Card.propTypes = {
  name: PropTypes.string.isRequired
};

export default Card;
