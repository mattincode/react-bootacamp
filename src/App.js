import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./App.css";
import Card from "./components/Card";

class App extends Component{
  render(){
    return(
      <div className="App">
        <h1> Hello, World! </h1>
        <Card></Card>
      </div>
    );
  }
}

export default hot(module)(App);